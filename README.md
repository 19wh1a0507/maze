# **Maze**

## Description
A Maze is given as NxN binary matrix of blocks where source block is the upper left most block i.e., maze[0][0] and destination block is lower rightmost block i.e., maze[N-1][N-1]. A person starts from source and has to reach the destination. The person can move in all four directions: UP ("U"), DOWN ("D"), LEFT ("L"), RIGHT ("R"). 

![](images/image_.jpg)

In the maze matrix, 0 means the block is a dead end and 1 means the block can be used in the path from source to destination.


## Game Demo

![](images/maze_demo.jpg)

If we reach the red block in the above figure(Destination) within 10 seconds then the victory screen appears else you lose and after a time delay we exit from the game window.

![](images/victory.png)
